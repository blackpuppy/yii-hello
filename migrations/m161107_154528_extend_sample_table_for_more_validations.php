<?php

use yii\db\Schema;
use yii\db\Migration;

class m161107_154528_extend_sample_table_for_more_validations extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->addColumn('{{%sample}}', 'email', Schema::TYPE_STRING . ' NOT NULL DEFAULT ""');
        $this->addColumn('{{%sample}}', 'url', Schema::TYPE_STRING . ' NOT NULL DEFAULT ""');
        $this->addColumn('{{%sample}}', 'filename', Schema::TYPE_STRING.' NOT NULL');
        $this->addColumn('{{%sample}}', 'avatar', Schema::TYPE_STRING.' NOT NULL');
    }

    public function down()
    {
        $this->dropColumn('{{%sample}}', 'email');
        $this->dropColumn('{{%sample}}', 'url');
        $this->dropColumn('{{%sample}}', 'filename');
        $this->dropColumn('{{%sample}}', 'avatar');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
