<?php

use yii\db\Migration;
use yii\db\Schema;

class m161101_083111_extend_user_table_for_role extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->addColumn('{{%user}}', 'role', Schema::TYPE_INTEGER.' NOT NULL DEFAULT 10');
    }

    public function down()
    {
        $this->dropColumn('{{%user}}', 'role');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
