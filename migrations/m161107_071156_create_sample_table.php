<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Handles the creation of table `sample`.
 */
class m161107_071156_create_sample_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%sample}}', [
            'id' => $this->primaryKey(),
            'thought' => Schema::TYPE_STRING.' NOT NULL DEFAULT ""',
            'goodness' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 0',
            'rank' => Schema::TYPE_INTEGER . ' NOT NULL',
            'censorship' => Schema::TYPE_STRING . ' NOT NULL',
            'occurred' => Schema::TYPE_DATE . ' NOT NULL',
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%sample}}');
    }
}
