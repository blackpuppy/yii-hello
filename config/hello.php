<?php

$hello = [
    'domains' => [
        'en' => [
            'language' => 'en-US',
            'name' => 'English',
            'domain' => 'en.hello.foreign-study.net',
        ],
        'cn' => [
            'language' => 'zh-CN',
            'name' => 'Simplified Chinese',
            'domain' => 'cn.hello.foreign-study.net',
        ],
        'tw' => [
            'language' => 'zh-TW',
            'name' => 'Traditional Chinese',
            'domain' => 'tw.hello.foreign-study.net',
        ],
        'jp' => [
            'language' => 'ja',
            'name' => 'Japanese',
            'domain' => 'jp.hello.foreign-study.net',
        ],
    ],
];

if (YII_ENV_DEV) {
    $hello['domains']['en']['domain'] = 'en.hello.local';
    $hello['domains']['cn']['domain'] = 'cn.hello.local';
    $hello['domains']['tw']['domain'] = 'tw.hello.local';
    $hello['domains']['jp']['domain'] = 'jp.hello.local';
}

return $hello;
