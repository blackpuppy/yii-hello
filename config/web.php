<?php

use yii\helpers\Url;

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'timeZone' => 'Asia/Singapore',
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'US5h4Xi8ndwUwlQgFIDmGMExR3qW_2Jp',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        // 'user' => [
        //     'identityClass' => 'app\models\User',
        //     'enableAutoLogin' => true,
        // ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'mailtrap.io',
                'username' => 'faa2bcdc35f934',
                'password' => 'e02e2b7e921b65',
                'port' => '2525',
                'encryption' => 'tls',
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning', 'info'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            // 'enableStrictParsing' => true,
            'rules' => [
                'status' => 'status/index',
                'status/index' => 'status/index',
                'status/create' => 'status/create',
                'status/view/<id:\d+>' => 'status/view',
                'status/update/<id:\d+>' => 'status/update',
                'status/delete/<id:\d+>' => 'status/delete',
                'status/<slug>' => 'status/slug',

                'status-log' => 'status-log/index',
                'status-log/index' => 'status-log/index',
                'status-log/create' => 'status-log/create',
                'status-log/view/<id:\d+>' => 'status-log/view',
                'status-log/update/<id:\d+>' => 'status-log/update',
                'status-log/delete/<id:\d+>' => 'status-log/delete',

                'defaultRoute' => '/site/index',
            ],
        ],
        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    //'basePath' => '@app/messages',
                    //'sourceLanguage' => 'en-US',
                    'fileMap' => [
                        'app' => 'app.php',
                        'app/error' => 'error.php',
                    ],
                ],
            ],
        ],
    ],
    'modules' => [
        'user' => [
            'class' => 'dektrium\user\Module',
            'enableUnconfirmedLogin' => true,
            'confirmWithin' => 21600,
            'cost' => 12,
            // 'modelMap' => [
            //     'User' => 'app\models\User',
            // ],
            'admins' => ['admin'],
        ],
        'redactor' => [
            'class' => 'yii\redactor\RedactorModule',
            'uploadDir' => '@webroot/uploads',
            'uploadUrl' => '/uploads',
            'imageAllowExtensions'=>['jpg','png','gif']
        ],
    ],
    'params' => $params,
    'on beforeRequest' => function ($event) {
        $domain = Url::to(['/'], true);
        $languageCode = substr($domain, 7, 2);
        $domains = Yii::$app->params['hello']['domains'];
        Yii::$app->language = $domains[$languageCode]['language'];
    },
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        'allowedIPs' => ['127.0.0.1', '192.168.56.*'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'allowedIPs' => ['127.0.0.1', '192.168.56.*'],
    ];
}

return $config;
