<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::t('app', 'My Company'),
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $navItems=[
        ['label' => Yii::t('app', 'Home'), 'url' => ['/site/index']],
        ['label' => Yii::t('app', 'Status'), 'url' => ['/status/index']],
        ['label' => Yii::t('app', 'Status Log'), 'url' => ['/status-log/index']],
        ['label' => Yii::t('app', 'Sample'), 'url' => ['/sample/index']],
        ['label' => Yii::t('app', 'About'), 'url' => ['/site/about']],
        ['label' => Yii::t('app', 'Contact'), 'url' => ['/site/contact']],
    ];
    if (Yii::$app->user->isGuest) {
        array_push(
            $navItems,
            ['label' => Yii::t('app', 'Sign In'), 'url' => ['/user/login']],
            ['label' => Yii::t('app', 'Sign Up'), 'url' => ['/user/register']]
        );
    } else {
        array_push($navItems,
            '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                Yii::t('app', 'Logout') . ' (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>'
        );
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $navItems,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <div class="container">
            <p class="pull-right">
                Language:
                <select id="language">
                    <?php
                        $domains = Yii::$app->params['hello']['domains'];
                        foreach ($domains as $key => $domain) {
                            echo '<option value="' . $key . '">' . $domain['name'] . '</option>';
                        }
                    ?>
                </select>
            </p>
        </div>
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
<script type="text/javascript">
$(function() {
    var switchLang = function(lang) {
        var csrfToken = $('meta[name="csrf-token"]').attr("content");
        $.ajax({
            url: '/lang/switch',
            type : 'POST',
            data: {language: lang, _csrf: csrfToken},
            success: function(data) {
                console.debug('/lang/switch success: data = ', data);

                if (data.redirect) {
                    window.location.href = data.url;
                }
            },
            error: function (jqXHR, textStatus, errorThrown){
                console.debug('/lang/switch error: ', errorThrown);
            },
            complete: function (jqXHR, textStatus){
                // console.debug('/lang/switch complete: textStatus = ', textStatus);
            }
        });
    }

    console.debug('language: <?php echo Yii::$app->language; ?>');
    console.debug('window.location: ', window.location);
    var currentLang = window.location.hostname.substring(0, 2);
    console.debug('currentLang: ', currentLang);
    $('#language').val(currentLang);

    $('#language').on('change', function(e) {
        e.preventDefault();
        var lang = this.value;
        console.debug('language selected: ', lang);
        // window.location.href = 'http://' + lang + window.location.href.substring(9);
        // window.location.href = '/lang/switch?language=' + lang;
        switchLang(lang);
    })
});
</script>
</body>
</html>
<?php $this->endPage() ?>
