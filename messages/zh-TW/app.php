<?php
/**
 * Message translations.
 *
 * This file is automatically generated by 'yii message/extract' command.
 * It contains the localizable messages extracted from source code.
 * You may modify this file by translating the extracted messages.
 *
 * Each array element represents the translation (value) of a message (key).
 * If the value is empty, the message is considered as not translated.
 * Messages that no longer need translation will have their translations
 * enclosed between a pair of '@@' marks.
 *
 * Message string can be used with plural forms format. Check i18n section
 * of the guide for details.
 *
 * NOTE: this file must be saved in UTF-8 encoding.
 */
return [
    'Censorship' => '審查',
    'Create Sample' => '創建樣本',
    'Create Status Log' => '創建狀態日誌',
    'Goodness' => '質量',
    'Occurred' => '日期',
    'Rank' => '等級',
    'Sample' => '樣本',
    'Samples' => '樣本',
    'Status ID' => '狀態 ID',
    'Status Log' => '狀態日誌',
    'Status Logs' => '狀態日誌',
    'Thought' => '想法',
    'Created By' => '創建用戶',
    'Updated By' => '更新用戶',
    'Login' => '@@登錄@@',
    'About' => '關於',
    'Are you sure you want to delete this item?' => '您確定要刪除這項嗎？ ',
    'Contact' => '聯繫',
    'Create' => '創建',
    'Create Status' => '創建狀態',
    'Created At' => '創建於',
    'Delete' => '刪除',
    'Home' => '主頁',
    'ID' => 'ID',
    'Logout' => '登出',
    'Message' => '消息',
    'My Company' => '我的公司',
    'Permissions' => '權限',
    'Remaining:' => '剩餘',
    'Reset' => '重置',
    'Search' => '搜索',
    'Sign In' => '登錄',
    'Sign Up' => '註冊',
    'Status' => '狀態',
    'Statuses' => '狀態',
    'Update' => '更新',
    'Update {modelClass}: ' => '更新 {modelClass}: ',
    'Updated At' => '更新於',
];
